package com.xgc.rtspconverterflvspringbootstarter;

import com.gc.easy.flv.util.FlvUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;

public class Test {

    public static void main(String[] args) throws Exception {

        String url = "rtmp://ns8.indexforce.com/home/mystream";
        String encodedUrl = java.net.URLEncoder.encode(url, "UTF-8");
        System.out.println("加密后：" + encodedUrl);
        String decodedUrl = java.net.URLDecoder.decode(encodedUrl, "UTF-8");
        System.out.println("解密后：" + decodedUrl);
    }
    public static void snap() throws Exception {
        byte[] flvImg = FlvUtil.getFlvImg("rtmp://liteavapp.qcloud.com/live/liteavdemoplayerstreamid");
        System.out.println("====");
        InputStream inputStream = new ByteArrayInputStream(flvImg);
        BufferedImage image = ImageIO.read(inputStream);

        String filePath = "/opt/image1.jpg";
        File outputFile = new File(filePath);
        ImageIO.write(image, "jpeg", outputFile);
    }
}
